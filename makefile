BIN_LIB=CMPSYS
LIBLIST=$(BIN_LIB) SAMPLE
SHELL=/QOpenSys/usr/bin/qsh

all: QDDSSRC.file SAMPLE.lib depts.sqlrpgle employees.sqlrpgle

QDDSSRC.file: CMPSYS.lib
depts.sqlrpgle: depts.dspf
employees.sqlrpgle: emps.dspf

%.lib:
	-system "DLTLIB LIB($*)"
	-system "CRTLIB LIB($*) TEXT('GIT DEMO')"

%.file:
	-system -qi "CRTSRCPF FILE($(BIN_LIB)/$*) RCDLEN(112)"

%.sqlrpgle:
	system -s "CHGATR OBJ('./qrpglesrc/$*.sqlrpgle') ATR(*CCSID) VALUE(1252)"
	liblist -a $(LIBLIST);\
#	system -s "CRTSQLRPGI OBJ($(BIN_LIB)/$*) SRCSTMF('./qrpglesrc/$*.sqlrpgle') COMMIT(*NONE) DBGVIEW(*SOURCE)"
	system "CRTSQLRPGI OBJ($(BIN_LIB)/$*) SRCSTMF('./qrpglesrc/$*.sqlrpgle') OBJTYPE(*PGM) COMMIT(*NONE) DBGVIEW(*SOURCE) REPLACE(*YES)"	

%.dspf:
	system "CPYFRMSTMF FROMSTMF('./qddssrc/$*.dspf') TOMBR('/QSYS.lib/$(BIN_LIB).lib/QDDSSRC.file/$*.mbr') MBROPT(*REPLACE)"
	system -s "CRTDSPF FILE($(BIN_LIB)/$*) SRCFILE($(BIN_LIB)/QDDSSRC) SRCMBR($*)"
	
	
all:
	@echo "Build finished!"
